# GitLab Dependency Check Parser

Transform OWASP Dependency Check JSON report into format to be ingested as GitLab dependency scan report.
